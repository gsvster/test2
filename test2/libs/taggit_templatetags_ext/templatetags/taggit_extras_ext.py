from django import template
from django.db import models
from django.db.models import Count
from django.contrib.auth.models import User

from templatetag_sugar.register import tag
from templatetag_sugar.parser import Name, Variable, Constant, Optional, Model

from taggit.managers import TaggableManager
from taggit.models import TaggedItem, Tag
from taggit_templatetags import settings
#from taggit_templatetags.templatetags.taggit_extras import get_weight_fun

from test2.apps.smurl.models import TaggedUser
from test2.apps.smurl.forms import TagsMultipleChoiceForm

from test2.utils.current_user import get_current_user

T_MAX = getattr(settings, 'TAGCLOUD_MAX', 6.0)
T_MIN = getattr(settings, 'TAGCLOUD_MIN', 0.0)

register = template.Library()

def get_weight_fun(t_min, t_max, f_min, f_max):
    def weight_fun(f_i, t_min=t_min, t_max=t_max, f_min=f_min, f_max=f_max):
        if f_max == f_min:
            mult_fac = float(t_max-t_min)/float(f_min)
        else:
            mult_fac = float(t_max-t_min)/float(f_max-f_min)
        return t_max - (f_max-f_i)*mult_fac
    return weight_fun

def get_queryset(forvar=None):
    user = User.objects.filter(username=get_current_user())
    if None == forvar:
        # get all tags
        #queryset = Tag.objects.all()
        user_tags = TaggedUser.objects.filter(username=user).values('tag__id').distinct()
        queryset = Tag.objects.filter(id__in = user_tags)
    else:
        # extract app label and model name
        beginning, applabel, model = None, None, None
        try:
            beginning, applabel, model = forvar.rsplit('.', 2)
        except ValueError:
            try:
                applabel, model = forvar.rsplit('.', 1)
            except ValueError:
                applabel = forvar

        # filter tagged items
        if applabel:
            queryset = TaggedItem.objects.filter(content_type__app_label=applabel.lower())
            #queryset = TaggedUser.objects.filter(content_type__app_label=applabel.lower())
        if model:
            queryset = queryset.filter(content_type__model=model.lower())

        # get tags
        tag_ids = queryset.values_list('tag_id', flat=True)
        queryset = Tag.objects.filter(id__in=tag_ids)

    num_times_sql = """
        SELECT COUNT(smurl_taggeduser.id)
        FROM smurl_taggeduser
        WHERE smurl_taggeduser.username_id = %(username)s
            AND smurl_taggeduser.tag_id=taggit_tag.id """ % \
                    {'username': user[0].pk}

    return queryset.extra(select={"num_times": num_times_sql})

@tag(register, [Constant('as'), Name(), Optional([Constant('for'), Variable()])])
def get_tagcloud_ext(context, asvar, forvar=None):
    queryset = get_queryset(forvar)
    num_times = queryset.values_list('num_times', flat=True)
    if(len(num_times) == 0):
        context[asvar] = queryset
        return ''
    weight_fun = get_weight_fun(T_MIN, T_MAX, min(num_times), max(num_times))
    queryset = queryset.order_by('name')
    for tag in queryset:
        tag.weight = weight_fun(tag.num_times)
    context[asvar] = queryset
    return ''

@tag(register, [ Variable('link_object'), Optional([Constant('as'), Name('asvar')])])
def get_tagform(context, link_object, asvar):
    context[asvar] = TagsMultipleChoiceForm(link_obj=link_object)
    return ''