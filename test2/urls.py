from django.conf.urls import patterns, include, url
#from django.views.generic import TemplateView
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^account/', include('test2.apps.account.urls')),
    url(r'^(?P<short_url>\w{4}\d{4})/$', 'test2.apps.smurl.views.redirect_short_url', name='redirect_short_url'),
    url(r'^$', 'test2.apps.smurl.views.homepage', name='homepage'),
    url(r'^my_links/$', 'test2.apps.smurl.views.my_links', name="account_my_links"),
    url(r'^delete_tag/$', 'test2.apps.smurl.views.delete_tag', name="smurl_delete_tag"),

)

if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
        url(r'^media/(?P<path>.*)$', 'serve'),
    )