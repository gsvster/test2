# -*- coding:utf-8 -*-

from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from test2.utils.views import render_to, render_json
from test2.apps.smurl.forms import UrlForm, TaggedUserForm, TagsMultipleChoiceForm
from test2.apps.smurl.models import Urls, UrlItems, TaggedUser
from taggit.models import Tag

def redirect_short_url(request, short_url):
    url = get_object_or_404(Urls, short_url__iexact=short_url)
    url.total_hits += 1
    url.save()
    if request.user.is_authenticated():
        urlItem = get_object_or_404(UrlItems, url=url)
        urlItem.total_hits += 1
        urlItem.save()
    return redirect(url.url)

@render_to('')
def homepage(request, template="smurl/homepage.html"):
    new_obj = None
    if request.method == "POST":
        form = UrlForm(request.POST, user=request.user)
        if form.is_valid():
            new_obj = form.save()
    else:
        form = UrlForm(user=request.user)

    return {
        'form':form,
        'short_url':new_obj,
    }, template

@render_to('')
def my_links(request, template="smurl/my_links.html"):
    links = UrlItems.objects.filter(username = request.user).order_by('-created')

    if request.method == "POST":
        if request.POST['form-type'] == u"add-new":
            form = TaggedUserForm(request.POST, current_user=request.user, user_tag=request.POST['user_tag_id'])
            if form.is_valid():
                form.save()
        else:
            form = TaggedUserForm(current_user=request.user)

        if request.POST['form-type'] == u"save-exist":
            link_obj = get_object_or_404(UrlItems, pk=int(request.POST['user_tag_id']))
            modify_form = TagsMultipleChoiceForm(request.POST, link_obj=link_obj, current_user=request.user)
            if modify_form.is_valid():
                modify_form.save()
        else:
            link_obj = get_object_or_404(UrlItems, pk=int(request.POST['user_tag_id']))
            modify_form = TagsMultipleChoiceForm(request.POST, link_obj=link_obj, current_user=request.user)

    else:
        form = TaggedUserForm(current_user=request.user)

    return {
        'links':links,
        'form':form,
    }, template

@login_required
@render_json
def delete_tag(request):
    result = list()
    tag = get_object_or_404(Tag, pk=int(request.POST.get('tag_id', '')))
    link = get_object_or_404(UrlItems, pk=int(request.POST.get('link_id', '')))
    usertag = get_object_or_404(TaggedUser, username=request.user, content_object=link, tag=tag)
    usertag.delete()
    return result
