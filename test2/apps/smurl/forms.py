# -*- coding:utf-8 -*-

from django import forms
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.core import validators
from test2.apps.smurl.models import Urls, UrlItems, TaggedUser
from test2.utils.current_user import get_current_user

from taggit.managers import TaggableManager
from taggit.models import TagBase, Tag

class UrlForm(forms.Form):
    url = forms.CharField(label='URL', max_length=200, widget=forms.TextInput(),required=True)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        if user and user.is_authenticated():
            self.username = user
        else:
            self.username = None
        super(UrlForm, self).__init__(*args, **kwargs)

    def clean_url(self):
        validate = URLValidator()
        value = self.cleaned_data["url"]
        if value:
            try:
                validate(value)
            except ValidationError:
                raise forms.ValidationError(u"Помилка в URL")
        return self.cleaned_data["url"]

    def save(self):
        url, url_created = Urls.objects.get_or_create(url=self.cleaned_data["url"])
        return UrlItems.objects.get_or_create(url=url, username=self.username)

class TaggedUserForm(forms.Form):
    tag = forms.CharField(label=u'Новий тег', max_length=200, widget=forms.TextInput(),required=True,error_messages={'required': 'Введіть назву тегу'})

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('current_user', None)
        self.user_tag = kwargs.pop('user_tag', None)

        if user.is_authenticated():
            self.username = user
        super(TaggedUserForm, self).__init__(*args, **kwargs)

    def clean_tag(self):
        return self.cleaned_data["tag"]

    def save(self):
        # TODO: insert messages
        url = UrlItems.objects.get(pk=int(self.user_tag))
        tag, tag_created = Tag.objects.get_or_create(name=self.cleaned_data["tag"])
        user_tag, user_tag_created = TaggedUser.objects.get_or_create(username=self.username, tag=tag, content_object=url )
        return user_tag

class DynamicMultipleChoiceField(forms.MultipleChoiceField):
    # The only thing we need to override here is the validate function.
    def validate(self, value):
        if self.required and not value:
            raise ValidationError(self.error_messages['required'])


class TagsMultipleChoiceForm(forms.Form):
    #tags = forms.ModelMultipleChoiceField(Tag.objects.all(), required=True, widget=forms.CheckboxSelectMultiple(), label='Теги')
    tags = forms.ModelMultipleChoiceField(
        Tag.objects.filter(id__in = TaggedUser.objects.filter(username=User.objects.filter(username=get_current_user())).values('tag__id').distinct()),
        required=True,
        widget=forms.CheckboxSelectMultiple(),
        label='Теги'
    )

    def __init__(self, *args, **kwargs):
        link_obj = kwargs.pop('link_obj', None)
        user = kwargs.pop('current_user', None)
        self.link_obj = link_obj
        if user and user.is_authenticated():
            self.username = user
        super(TagsMultipleChoiceForm, self).__init__(*args, **kwargs)

        user_tags = TaggedUser.objects.filter(content_object=link_obj).values('tag__id').distinct()
        self.fields['tags'].initial = [t.pk for t in Tag.objects.filter(id__in = user_tags)]

    def clean_tags(self):
        return self.cleaned_data["tags"]

    def save(self):
        tags = TaggedUser.objects.filter(content_object=self.link_obj).delete()

        for t in self.cleaned_data["tags"]:
            TaggedUser.objects.create(tag=t, username=self.username,content_object=self.link_obj)
