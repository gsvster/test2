# -*- coding:utf-8 -*-

from django.contrib import admin
from test2.apps.smurl.models import Urls, UrlItems

class UrlsAdmin(admin.ModelAdmin):
    pass

class UrlItemsAdmin(admin.ModelAdmin):
    pass
    #def save_model(self, request, obj, form, change):
    #    if getattr(obj, 'author', None) is None:
    #        obj.author = request.user
    #    obj.save()

admin.site.register(Urls, UrlsAdmin)
admin.site.register(UrlItems, UrlItemsAdmin)