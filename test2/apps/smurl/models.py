# -*- coding:utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase
from random import choice, seed
from test2.utils.current_user import get_current_user
seed()

def random_string(char_length=4, digit_length=4):
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    digit = "1234567890"
    pass_str_char = ''.join([choice(chars) for i in range(char_length)])
    pass_str_digit = ''.join([choice(digit) for i in range(digit_length)])
    return pass_str_char + pass_str_digit

class Urls(models.Model):
    url = models.URLField(verbose_name='Full URL', max_length=300)
    short_url = models.CharField(verbose_name='Short URL', max_length=8, blank=True, null=True)
    total_hits = models.IntegerField(verbose_name='Total hits', default=0)
    created = models.DateTimeField(verbose_name='Created', auto_now_add=True)

    class Meta:
        verbose_name = 'Url'
        verbose_name_plural = 'Urls'
        ordering = ('created',)

    def __unicode__(self):
        return u'%s' % self.url

    def save(self, *args, **kwargs):
        if not self.short_url:
            self.short_url = random_string()
        super(Urls, self).save(*args, **kwargs)

class TaggedUser(TaggedItemBase):
    username = models.ForeignKey(User, blank=False, null=False, verbose_name='Username')
    content_object = models.ForeignKey('UrlItems')

    def save(self, *args, **kwargs):
        self.username = User.objects.get(username=get_current_user())
        super(TaggedUser, self).save(*args, **kwargs)

class UrlItems(models.Model):
    url = models.ForeignKey(Urls)
    total_hits = models.IntegerField(verbose_name='Total hits', default=0)
    username = models.ForeignKey(User, blank=True, null=True, verbose_name='Username')
    tags = TaggableManager(through=TaggedUser)
    created = models.DateTimeField(verbose_name='Created', auto_now_add=True)
    last_redirect = models.DateTimeField(verbose_name='Last redirect', auto_now_add=True)

    class Meta:
        verbose_name = 'Url item'
        verbose_name_plural = 'Url items'
        ordering = ('created',)

    def __unicode__(self):
        return u'%s (%s)' % (self.url, self.username)

    def short_absolute_url(self):
        return u"http://%(site)s/%(rand_url)s" % {
            'site': Site.objects.get_current(),
            'rand_url': self.url.short_url,
        }
