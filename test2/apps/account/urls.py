
from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^login/$', 'test2.apps.account.views.login', name="account_login"),
    url(r'^signup/$', 'test2.apps.account.views.signup', name="account_signup"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {"template_name": "account/logout.html"}, name="account_logout"),
)
