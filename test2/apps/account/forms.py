# -*- coding:utf-8 -*-

import re
from django import forms
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.contrib.auth.models import User

alnum_re = re.compile(r'^\w+$')

class SignupForm(forms.Form):
    username = forms.CharField(label="Login", max_length=30, widget=forms.TextInput())
    password1 = forms.CharField(label="Password", widget=forms.PasswordInput(render_value=False))
    password2 = forms.CharField(label="Password (again)", widget=forms.PasswordInput(render_value=False))
    email = forms.EmailField(label = "E-mail", required=True, widget = forms.TextInput())

    def clean_username(self):
        if not alnum_re.search(self.cleaned_data["username"]):
            raise forms.ValidationError(_("Usernames can only contain letters, numbers and underscores."))
        try:
            user = User.objects.get(username__iexact=self.cleaned_data["username"])
        except User.DoesNotExist:
            return self.cleaned_data["username"]
        raise forms.ValidationError("Login already used")

    def clean(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
                raise forms.ValidationError("Passwords is not same")
        return self.cleaned_data

    def save(self):
        username = self.cleaned_data["username"]
        email = self.cleaned_data["email"]
        password = self.cleaned_data["password1"]

        new_user = User.objects.create_user(username, email, password)
        new_user.is_active = True
        new_user.save()

        return username, password # required for authenticate()

class LoginForm(forms.Form):
    username = forms.CharField(label="Username", max_length=30, widget=forms.TextInput())
    password = forms.CharField(label="Password", widget=forms.PasswordInput(render_value=False))
    remember = forms.BooleanField(label="Remember me", required=False)
    user = None

    def clean(self):
        if self._errors:
            return
        user = authenticate(username=self.cleaned_data["username"], password=self.cleaned_data["password"])
        if user:
            if user.is_active:
                self.user = user
            else:
                raise forms.ValidationError("This account is not active")
        else:
            raise forms.ValidationError("Login or/and password is not valid")
        return self.cleaned_data

    def login(self, request):
        if self.is_valid():
            login(request, self.user)
            messages.add_message(request, messages.INFO, 'You are logged in')
            if self.cleaned_data['remember']:
                request.session.set_expiry(60 * 60 * 24 * 7 * 3)
            else:
                request.session.set_expiry(0)
            return True
        return False

