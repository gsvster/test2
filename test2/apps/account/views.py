# -*- coding:utf-8 -*-

from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from test2.utils.views import render_to
from test2.apps.account.forms import LoginForm, SignupForm
from test2.apps.smurl.models import UrlItems

@render_to('')
def login(request, success_url="/", url_required=False, template="account/login.html"):
    if request.method == "POST" and not url_required:
        form = LoginForm(request.POST)
        if form.login(request):
            return HttpResponseRedirect(success_url)
    else:
        form = LoginForm()

    return {
        'form':form,
    }, template

@render_to('')
def signup(request, form_class=SignupForm, template="account/signup.html"):
    if request.method == "POST":
        form = form_class(request.POST)
        if form.is_valid():
            username, password = form.save()
            user = authenticate(username=username, password=password)
            auth_login(request, user)
            messages.add_message(request, messages.INFO, 'You are logged in')
            return HttpResponseRedirect("/")
    else:
        form = form_class()

    return {
        'form':form,
    }, template
